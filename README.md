#Iso Dark theme for Godot 3

This is an updated version of the theme of the same name by GalanCM,
 which was for Godot 2. 

All common elements are working and this should be suitable for most
 games. Controls inside of a tree control are not themed, as well 
as a few other things like the color picker. See the readme in the 
[upstream repo](https://github.com/GalanCM/Iso-Themes) for more 
details.
